package temperatura;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TemperaturaTransformerFailTest {
	
	private double celsius;
	private double fahrenheit;
	private TemperatureTransformer transformer;
	
	public TemperaturaTransformerFailTest(double celsius, double fahrenheit){
		this.celsius = celsius;
		this.fahrenheit = fahrenheit;
	}
	
	@Before
	public void setUp() {
		transformer = new TemperatureTransformer();
	}
	
	@Parameters
	public static Collection<Double[]> values(){
		return Arrays.asList(new Double[][]{ { -274.0, -461.2}} );
	}
	
	@Test
	public void converterPraFahrenheit(){		
		CelsiusTemperature celsiusTemperature = new CelsiusTemperature();
		Temperature temperature = null;
		try {
			celsiusTemperature.setValue(this.celsius);
			fail("Valor menor que o zero absoluto, deveria ter falhado.");
			temperature = transformer.convert(celsiusTemperature);					
		} catch (Exception e) {
			assertEquals(null, temperature);		
		}
	}
	
	@Test
	public void converterPraCelsius(){
		FahrenheitTemperature fahrenheitTemperature = new FahrenheitTemperature();
		Temperature temperature = null;
		try{
			fahrenheitTemperature.setValue(this.fahrenheit);
			fail("Valor menor que o zero absoluto, deveria ter falhado.");
			temperature = transformer.convert(fahrenheitTemperature);			
		}catch(Exception e){
			assertEquals(null, temperature);
		}
		
	}

}
