package temperatura;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TemperaturaTransformerTest {

	private double celsius;
	private double fahrenheit;
	private TemperatureTransformer transformer;

	public TemperaturaTransformerTest(double celsius, double fahrenheit) {
		this.celsius = celsius;
		this.fahrenheit = fahrenheit;
	}

	@Before
	public void setUp() {
		transformer = new TemperatureTransformer();
	}

	@Parameters
	public static Collection<Double[]> values() {
		return Arrays.asList(new Double[][] { { 3.0, 37.4 }, { 60.0, 140.0 }, { -30.0, -22.0 }, { 0.0, 32.0 } });
	}

	@Test
	public void celsiusToFahrenheitTest() throws Exception {
		CelsiusTemperature cTemp = new CelsiusTemperature();
		cTemp.setValue(this.celsius);
		assertEquals(this.fahrenheit, transformer.convert(cTemp).getValue(), 0.0);
	}

	@Test
	public void fahrenheitToCelsiusTest() throws Exception {
		FahrenheitTemperature fTemp = new FahrenheitTemperature();
		fTemp.setValue(this.fahrenheit);
		assertEquals(this.celsius, transformer.convert(fTemp).getValue(), 0.01);
	}
}
